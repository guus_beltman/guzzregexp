﻿using System;
using GuzzRegExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GuzzRegExpTest
{
    [TestClass]
    public class EmailTest
    {

        [TestMethod]
        public void ValidateEmailAddressStrict()
        {
            Assert.IsTrue(Email.ValidateEmailAddressStrict("name@gmail.com"), "basic gmail address");
            Assert.IsTrue(Email.ValidateEmailAddressStrict("first.name@gmail.com"), "basic gmail address with dot");
        }
    }
}

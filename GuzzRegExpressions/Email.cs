﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GuzzRegExpressions
{
    public class Email
    {
        private static readonly Regex RegexEmailAddressStrict = new Regex(@"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([a-zA-Z]{2,4})|(([aA][eE][rR][oO])|([cC][oO]{2}[Pp])|([iI][nN][fF][oO])|([mM][uU][sS][eE][uU][mM])|([nN][aA][mM][eE])))$");

        /// <summary>
        /// Validate e-mail addresses in a strict way
        /// </summary>
        /// <param name="pEmailAddress"></param>
        /// <returns></returns>
        public static bool ValidateEmailAddressStrict(string pEmailAddress)
        {
            return RegexEmailAddressStrict.IsMatch(pEmailAddress);
        }
    }
}
